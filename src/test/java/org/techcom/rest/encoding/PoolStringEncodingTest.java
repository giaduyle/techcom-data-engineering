package org.techcom.rest.encoding;

import java.util.Arrays;

import javax.inject.Inject;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.techcom.rest.model.Pool;

@QuarkusTest
public class PoolStringEncodingTest {

    @Inject
    PoolStringEncoding encoderDecoder;

    @Test
    public void testEncode() {
        Pool pool = new Pool(1);
        pool.addAll(Arrays.asList(1, 3, 2, 4));

        String poolString = encoderDecoder.encode(pool);
        Assertions.assertEquals("1;1,2,3,4", poolString);
    }

    @Test
    public void testDecode() {
        String poolString = "1;1,3,2,4";

        Pool pool = encoderDecoder.decode(poolString);

        Assertions.assertNotNull(pool);
        Assertions.assertEquals(1, pool.getId());
        Assertions.assertEquals(4, pool.size());
    }
}
