package org.techcom.rest.repository;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

import javax.inject.Inject;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.techcom.rest.config.AppConfig;

@QuarkusTest
public class PoolRepositoryTest {

    @Inject
    PoolRepository repository;

    @Inject
    AppConfig appConfig;

    @BeforeEach
    public void setup() {
        repository.clear();
    }

    @Test
    public void testInit() {
        appConfig.setPoolFile(Paths.get("src", "test", "resources", "test-data", "pools.txt").toString());
        repository.init();

        Assertions.assertNotNull(repository.findPoolById(1));
        Assertions.assertNotNull(repository.findPoolById(2));
        Assertions.assertNotNull(repository.findPoolById(3));
    }

    @Test
    public void testDestroy() {
        Path destFile = Paths.get("target", "save-pools.txt");
        appConfig.setPoolFile(destFile.toString());

        repository.createPool().addAll(Arrays.asList(1, 2, 3, 4, 5));
        repository.destroy();

        Assertions.assertTrue(Files.exists(destFile));
    }

}
