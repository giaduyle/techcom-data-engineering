package org.techcom.rest.api;

import java.util.Arrays;

import javax.inject.Inject;
import javax.ws.rs.core.MediaType;

import io.quarkus.test.junit.QuarkusTest;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.techcom.rest.api.dto.request.AppendRequest;
import org.techcom.rest.model.Pool;
import org.techcom.rest.repository.PoolRepository;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

@QuarkusTest
@Slf4j
public class PercentileUpdateApiTest {

    @Inject
    PoolRepository poolRepository;

    @BeforeEach
    public void setup() {
        poolRepository.clear();
    }

    @Test
    public void testInsert() {
        given()
            .contentType(MediaType.APPLICATION_JSON)
            .body(AppendRequest.builder()
                .poolId(1)
                .poolValues(Arrays.asList(1, 7, 2, 6))
                .build())
            .when()
            .post("/tech-com/api/v1/pools")
            .then()
            .statusCode(200)
            .body("status", equalTo("Inserted"), "poolId", equalTo(1));
    }

    @Test
    public void testAppend() {
        Pool pool = poolRepository.createPool(2);
        pool.addAll(Arrays.asList(7, 6, 8));

        given()
            .contentType(MediaType.APPLICATION_JSON)
            .body(AppendRequest.builder()
                .poolId(2)
                .poolValues(Arrays.asList(1, 2))
                .build())
            .when()
            .post("/tech-com/api/v1/pools")
            .then()
            .statusCode(200)
            .body("status", equalTo("Appended"), "poolId", equalTo(2));

        Assertions.assertEquals(5, pool.size());
    }

}
