package org.techcom.rest.api;

import java.util.Arrays;

import javax.inject.Inject;
import javax.ws.rs.core.MediaType;

import io.quarkus.test.junit.QuarkusTest;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.techcom.rest.api.dto.request.QueryRequest;
import org.techcom.rest.model.Pool;
import org.techcom.rest.repository.PoolRepository;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

@QuarkusTest
@Slf4j
public class PercentileQueryApiTest {

    @Inject
    PoolRepository poolRepository;

    @BeforeEach
    public void setup() {
        poolRepository.clear();
    }

    @Test
    public void testQuery() {
        Pool pool = poolRepository.createPool();
        pool.addAll(Arrays.asList(24, 2, 5, 10));

        given()
            .contentType(MediaType.APPLICATION_JSON)
            .body(QueryRequest.builder()
                .poolId(1)
                .percentile(99.5)
                .build())
            .when()
            .post("/tech-com/api/v1/pools/query")
            .then()
            .statusCode(200)
            .body(
                "poolId", equalTo(pool.getId()),
                "totalCount", equalTo(pool.size()),
                "value", equalTo(24.0F)
            );
    }

}
