package org.techcom.rest.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class PoolTest {

    @Test
    public void testIterator() {
        Pool pool = new Pool(123);
        pool.addAll(Arrays.asList(11, 1, 6, 4, 5, 100, 6));

        List<Integer> values = new ArrayList<>(pool.size());
        for (Integer value : pool) {
            values.add(value);
        }

        Assertions.assertEquals(Arrays.asList(1, 4, 5, 6, 11, 100), values);
    }

}
