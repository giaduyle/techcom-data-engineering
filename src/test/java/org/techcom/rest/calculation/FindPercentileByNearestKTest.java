package org.techcom.rest.calculation;

import java.util.Arrays;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.techcom.rest.model.Pool;

public class FindPercentileByNearestKTest {

    @Test
    public void testFindPercentile() {
        Pool pool = new Pool(123);
        pool.addAll(Arrays.asList(20, 40, 35, 15, 40, 50));

        PoolCalculator calculator = new NearestRankPercentileCalculator();

        Assertions.assertEquals(15, calculator.calculateValue(pool, 5));
        Assertions.assertEquals(20, calculator.calculateValue(pool, 30));
        Assertions.assertEquals(35, calculator.calculateValue(pool, 40));
        Assertions.assertEquals(40, calculator.calculateValue(pool, 70));
        Assertions.assertEquals(50, calculator.calculateValue(pool, 99.5));
        Assertions.assertEquals(50, calculator.calculateValue(pool, 100));
    }

}
