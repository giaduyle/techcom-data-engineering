package org.techcom.rest.model;

import java.util.Collection;
import java.util.Iterator;
import java.util.NavigableSet;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * @author duy.legia
 */
@RequiredArgsConstructor
public class Pool implements Iterable<Integer> {

    @Getter
    private final Integer id;

    private final NavigableSet<Integer> values = new ConcurrentSkipListSet<>();

    private AtomicInteger size = new AtomicInteger();

    public boolean add(Integer value) {
        if (values.add(value)) {
            size.getAndIncrement();
            return true;
        }
        return false;
    }

    public boolean addAll(Collection<Integer> valueList) {
        if (values.addAll(valueList)) {
            size.addAndGet(valueList.size());
            return true;
        }
        return false;
    }

    public boolean remove(Integer value) {
        if (values.remove(value)) {
            size.decrementAndGet();
            return true;
        }
        return false;
    }

    public int size() {
        return size.get();
    }

    public Stream<Integer> stream() {
        return values.stream();
    }

    @Override
    public Iterator<Integer> iterator() {
        return values.iterator();
    }

    public Iterator<Integer> descendingIterator() {
        return values.descendingIterator();
    }

}
