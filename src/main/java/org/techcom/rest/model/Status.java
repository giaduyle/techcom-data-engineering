package org.techcom.rest.model;

/**
 * @author duy.legia
 */

public enum Status {

    Inserted,
    Appended,
    Failed_to_add_values,
    Not_existed;

}
