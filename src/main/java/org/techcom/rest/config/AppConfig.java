package org.techcom.rest.config;

import javax.inject.Singleton;

import lombok.Data;
import org.eclipse.microprofile.config.inject.ConfigProperty;

/**
 * @author duy.legia
 */
@Data
@Singleton
public class AppConfig {

    @ConfigProperty(name = "pool.file")
    String poolFile;

}
