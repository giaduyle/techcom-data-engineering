package org.techcom.rest.persistence;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.Collections;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.inject.Inject;
import javax.inject.Singleton;

import lombok.extern.slf4j.Slf4j;
import org.techcom.rest.config.AppConfig;
import org.techcom.rest.encoding.PoolStringEncoding;
import org.techcom.rest.model.Pool;

/**
 * @author duy.legia
 */
@Singleton
@Slf4j
public class FilePoolPersistenceStorage implements PoolPersistenceStorage {

    @Inject
    AppConfig config;

    @Inject
    PoolStringEncoding encoderDecoder;

    @Override
    public void write(Collection<Pool> pools) {
        Path path = Paths.get(config.getPoolFile());
        log.info("Persisting {} pools to file {}", pools.size(), path);
        try (OutputStream outputStream = new FileOutputStream(path.toFile())) {
            for (Pool pool : pools) {
                String poolString = encoderDecoder.encode(pool);
                outputStream.write(poolString.getBytes(StandardCharsets.UTF_8));
            }
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public Collection<Pool> load() {
        Path path = Paths.get(config.getPoolFile());
        if (!Files.exists(path)) {
            log.info("Not loading any pools due to not found pool file {}", path);
            return Collections.emptyList();
        }
        log.info("Loading pools from file {}", path);
        try (Stream<String> lines = Files.lines(path)) {
            return lines
                .filter(Objects::nonNull)
                .map(line -> encoderDecoder.decode(line))
                .collect(Collectors.toList());
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }
}
