package org.techcom.rest.persistence;

import java.util.Collection;

import org.techcom.rest.model.Pool;

/**
 * A pool persistence storage. It can be a local file or a remote database depending on the implementation.
 *
 * @author duy.legia
 */
public interface PoolPersistenceStorage {

    /**
     * Write pools
     *
     * @param pools a collection of {@link Pool}
     */
    void write(Collection<Pool> pools);

    /**
     * Load pools
     *
     * @return a collection of {@link Pool}
     */
    Collection<Pool> load();

}
