package org.techcom.rest.encoding;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Singleton;

import org.techcom.rest.model.Pool;

import static java.util.stream.Collectors.joining;

/**
 * @author duy.legia
 */
@Singleton
public class PoolStringEncoding {

    private static final String POOL_ID_DELIMITER = ";";
    private static final String POOL_VALUES_DELIMITER = ",";

    public String encode(Pool pool) {
        String poolValues = pool.stream().map(Object::toString).collect(joining(POOL_VALUES_DELIMITER));
        return String.join(POOL_ID_DELIMITER, pool.getId().toString(), poolValues);
    }

    public Pool decode(String value) {
        if (value == null || value.isEmpty()) {
            throw new IllegalArgumentException("Null/empty pool string format");
        }
        String[] poolParts = value.split(POOL_ID_DELIMITER);
        if (poolParts.length < 2) {
            throw new IllegalArgumentException("Invalid pool string format");
        }
        Integer id = Integer.parseInt(poolParts[0]);
        String[] poolValues = poolParts[1].split(POOL_VALUES_DELIMITER);
        List<Integer> intValues = Arrays.stream(poolValues).map(Integer::parseInt).collect(Collectors.toList());
        Pool pool = new Pool(id);
        pool.addAll(intValues);
        return pool;
    }

}
