package org.techcom.rest;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import io.quarkus.runtime.ShutdownEvent;
import lombok.extern.slf4j.Slf4j;
import org.techcom.rest.repository.PoolRepository;

/**
 * @author duy.legia
 */
@ApplicationPath("/tech-com/api")
@ApplicationScoped
@Slf4j
public class TechcomApp extends Application {

    @Inject
    PoolRepository poolRepository;

    void onStop(@Observes ShutdownEvent event) {
        poolRepository.destroy();
    }

}