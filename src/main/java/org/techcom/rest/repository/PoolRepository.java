package org.techcom.rest.repository;

import java.util.Collection;
import java.util.Optional;
import java.util.concurrent.ConcurrentNavigableMap;
import java.util.concurrent.ConcurrentSkipListMap;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Inject;
import javax.inject.Singleton;

import lombok.extern.slf4j.Slf4j;
import org.techcom.rest.model.Pool;
import org.techcom.rest.persistence.PoolPersistenceStorage;

/**
 * @author duy.legia
 */
@Singleton
@Slf4j
public class PoolRepository {

    @Inject
    PoolPersistenceStorage persistenceStorage;

    private ConcurrentNavigableMap<Integer, Pool> poolMap = new ConcurrentSkipListMap<>();

    @PostConstruct
    public void init() {
        clear();
        load();
    }

    @PreDestroy
    public void destroy() {
        save();
        clear();
    }

    public Optional<Pool> findPoolById(int id) {
        return Optional.ofNullable(poolMap.get(id));
    }

    public Pool createPool(int id) {
        Pool pool = new Pool(id);
        poolMap.put(id, pool);
        return pool;
    }

    public Pool createPool() {
        int id = nextPoolId();
        return createPool(id);
    }

    public void clear() {
        poolMap.clear();
    }

    private void save() {
        persistenceStorage.write(poolMap.values());
    }

    private void load() {
        Collection<Pool> pools = persistenceStorage.load();
        for (Pool pool : pools) {
            Integer poolId = pool.getId();
            if (poolMap.containsKey(poolId)) {
                throw new IllegalStateException("Pool with id " + poolId + " already loaded/existed!");
            }
        }
    }

    private int nextPoolId() {
        if (poolMap.isEmpty()) {
            return 1;
        }
        return poolMap.lastKey() + 1;
    }

}
