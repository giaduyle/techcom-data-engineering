package org.techcom.rest.calculation;

import org.techcom.rest.model.Pool;

/**
 * @author duy.legia
 */
public interface PoolCalculator {

    /**
     * Calculate a value with percentile in pool.
     *
     * @param pool  {@link Pool}
     * @param value percentile value
     * @return a value
     */
    Double calculateValue(Pool pool, double value);

}
