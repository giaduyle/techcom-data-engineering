package org.techcom.rest.calculation;

import java.util.Iterator;

import javax.enterprise.inject.Default;
import javax.inject.Singleton;

import org.techcom.rest.model.Pool;

/**
 * Calculate the percentile and find the ranked value in pool.
 * <p>This class implements the Nearest-rank method.
 * <p>It guarantees that the percentile value is existed in the ordinal list.
 *
 * @author duy.legia
 * @see <a href="https://en.wikipedia.org/wiki/Percentile">Percentile-Wiki</a>
 */
@Singleton
@Default
public class NearestRankPercentileCalculator implements PoolCalculator {

    @Override
    public Double calculateValue(Pool pool, double value) {
        int ordinalRank = calculateOrdinalRank(pool.size(), value);
        double midPoint = Math.floor((double)pool.size() / 2);

        Iterator<Integer> iterator;
        int loop;
        if (ordinalRank <= midPoint) {
            iterator = pool.iterator();
            loop = ordinalRank;
        } else {
            iterator = pool.descendingIterator();
            loop = pool.size() - ordinalRank + 1;
        }

        Double result = null;
        while (loop > 0) {
            result = (double)iterator.next();
            loop--;
        }

        return result;
    }

    private int calculateOrdinalRank(int size, double percentile) {
        return (int)Math.ceil(percentile / 100.0 * size);
    }
}
