package org.techcom.rest.api;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.techcom.rest.api.dto.request.AppendRequest;
import org.techcom.rest.api.dto.response.AppendResponse;
import org.techcom.rest.model.Pool;
import org.techcom.rest.model.Status;
import org.techcom.rest.repository.PoolRepository;

/**
 * @author duy.legia
 */
@Path("/v1/pools")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class PercentileUpdateApi {

    @Inject
    PoolRepository poolRepository;

    /**
     * Persist a {@link Pool}. If already exists then append the values to the pool
     *
     * @param request {@link AppendRequest} including pool id and appending values
     * @return {@link AppendResponse}
     */
    @POST
    public AppendResponse append(AppendRequest request) {
        int poolId = request.getPoolId();
        AppendResponse.AppendResponseBuilder builder = AppendResponse.builder();
        builder.poolId(poolId);
        builder.status(Status.Appended);
        Pool pool = poolRepository.findPoolById(poolId).orElseGet(() -> {
            builder.status(Status.Inserted);
            return poolRepository.createPool(poolId);
        });
        if (!pool.addAll(request.getPoolValues())) {
            builder.status(Status.Failed_to_add_values);
        }
        return builder.build();
    }

}
