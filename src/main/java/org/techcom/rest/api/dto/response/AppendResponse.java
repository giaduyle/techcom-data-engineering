package org.techcom.rest.api.dto.response;

import io.quarkus.runtime.annotations.RegisterForReflection;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.techcom.rest.model.Status;

/**
 * @author duy.legia
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@RegisterForReflection
public class AppendResponse {

    private Integer poolId;

    private Status status;

    private String errorDetails;

}
