package org.techcom.rest.api;

import java.util.Optional;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.techcom.rest.api.dto.request.QueryRequest;
import org.techcom.rest.api.dto.response.QueryResponse;
import org.techcom.rest.calculation.PoolCalculator;
import org.techcom.rest.model.Pool;
import org.techcom.rest.model.Status;
import org.techcom.rest.repository.PoolRepository;

/**
 * @author duy.legia
 */
@Path("/v1/pools/query")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class PercentileQueryApi {

    @Inject
    PoolRepository poolRepository;

    @Inject
    PoolCalculator poolCalculator;

    /**
     * Query percentile of a {@link Pool}
     *
     * @param request {@link QueryRequest} including pool id and percentile value
     * @return {@link QueryResponse}
     */
    @POST
    public QueryResponse query(QueryRequest request) {
        Optional<Pool> poolOptional = poolRepository.findPoolById(request.getPoolId());
        QueryResponse.QueryResponseBuilder builder = QueryResponse.builder();
        if (poolOptional.isEmpty()) {
            builder.status(Status.Not_existed);
        } else {
            Pool pool = poolOptional.get();
            builder
                .poolId(pool.getId())
                .totalCount(pool.size())
                .value(poolCalculator.calculateValue(pool, request.getPercentile()));
        }
        return builder.build();
    }

}
