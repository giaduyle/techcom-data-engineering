# Techcom Quantile Calculation Project

This project uses Quarkus, the Supersonic Subatomic Java Framework.
If you want to learn more about Quarkus, please visit its website: https://quarkus.io/ .

## Ideas
* This project uses Quarkus as the develoment framework. 
    * Quarkus is a new high performance Java frameworks to build Microservices.
    * Quarkus has custom tailored JVM named GraalVM that is said to be lightweight and fast 
        thanks to its compilation to native codes.
    * Quarkus can compile Java codes to native image in a docker image for native performances.
    
* Authentication
    * This implementation does not come with an authentication API. 
        * It is best to implement an authentication service separately and use it to wall and grant users permissions 
    to visit APIs. 
        * Nowadays, we can choose to implement 
    a simple JWT https://quarkus.pro/guides/security-jwt.html
    or a full OAuth 2.0 https://quarkus.io/guides/security-oauth2 flow depending on requirements.

* Percentile calculation, 
    * This application comes with the default Nearest Rank Method (NRM) https://en.wikipedia.org/wiki/Percentile. 
        It calculates the ordinal rank of the percentile and extract ordinal elements 
        from an internal ordered set of the pool.
    * To keep the ordered ordinal elements of the pool each time new values are inserted,
    the default pool implementation chooses Skip-list set to store the values.
        * The skip-list implementation guarantees O(logn) time complexity to insert and delete values.
        * Once the ordinal rank is calculated, the value is retrieved via stepping through half of the list each time.
            So the overall time complexity to search for values is O(n/2). 
            But the real use cases are usually interested in 90-99.9 percentile range, so  we balance the complexity.
            
    * Another approach to calculate NRM is to employ an array list but sort it after each insertion or batch insertions. 
        * In our app, the pool contains mostly primitive integer values, 
    therefore Java will choose QuickSort (Unstable sort algorithm), 
    but the its worst case happens mostly due to wrong pivot choices and an already sorted array requires shuffling.
    
* Availability & Scalability & Resiliency
    * With Quarkus native support and a docker image build mechanism, our app can benefit from containerized deployment
    with some API gateway or Proxies receiving and routing requests and a good container orchestration tooling, 
    we can ensure high availability and easy horizontal scaling of the app.
    With scaling out, we have a problem with consistency of persisting pools. 
    In this case, we can consider a cache-first design or a distributed queueing system with a transactional database.
    
    * Our app has a feature to save/load pool encoded values in text files to ensure resiliency in case of downtime. 
    The save will be triggered upon app shutting down.
    The load will be called when the app is starting up.
    We can do better by adding interval writing to files with a scheduler.
    Or we can design a queueing system to produce/consume events of updating pools to store data 
    with a stronger fault-tolerant manner.
 

# Requirements
* JDK 11+
* Maven 3.8+

## Running the application in dev mode

You can run your application in dev mode that enables live coding using:
`./mvnw compile quarkus:dev`

## Packaging and running the application

The application can be packaged using:
`./mvnw package`

It produces the `quarkus-run.jar` file in the `target/quarkus-app/` directory.
Be aware that it’s not an _uber-jar_ as the dependencies are copied into the `target/quarkus-app/lib/` directory.

If you want to build an _uber-jar_, execute the following command:
`./mvnw package -Dquarkus.package.type=uber-jar`

The application is now runnable using `java -jar target/quarkus-app/quarkus-run.jar`.

## The app comes with a Swagger UI
Boot the app in dev or prod mode, then access it via
 `http://localhost:8080/q/swagger-ui`